# VoonyxChallenge-SpringBatch

Ce petit défi technique est assez simple, mais permet de valider les connaissances et la compréhension générale d'un projet SpringBoot/SpringBatch.
Le projet était fonctionnel à la base, mais il a été modifié pour ne plus fonctionner et le but est de le rendre fonctionnel à nouveau.

En gros, le projet consiste en un batch Spring qui lit des données dans une base de données et produit 2 rapports en format CSV.
Au démarrage du batch, Liquibase est responsable de créer le schéma de données principal en créant les différentes tables ainsi que 
les données dans chacune d'elles.  La structure de données est très simple, il existe des concessionnaires automobile situés dans 
une région donnée (Québec ou Montréal) et ces concessionnaires ont plusieurs automobiles à vendre.

Le premier rapport produit par le batch liste toutes les voitures disponibles chez le concessionnaire "Quebec Subaru", 
alors que le second rapport liste tous les véhicules qui ont une valeur de vente plus élevée que 45,000$.

Pour ce projet, nous vous demandons les choses suivantes:

	- Utiliser 2 BD différentes :
		- Une première pour stocker les métadonnées du batch Spring (voonyxChallengeMeta)
		- Une seconde pour stocker les données utilisées pour produire les 2 rapports (voonyxChallenge)
	- Utiliser Liquibase pour créer le schéma de données et créer les données
	- Le batch doit exécuter les 2 rapports en une seule exécution
	- Lors de la recherche du concessionnaire, le nom du concessionnaire doit être utilisé pour la recherche (et non pas le ID)
	- Tous les paramètres passés au batch doivent provenir de propriétés d'environnement
	- Aucune modification ne doit être apportée à la structure de données produite par Liquibase
	- Une fois le schéma de données créé, une validation JPA du schéma doit être faite au démarrage
	

Le projet est très simple et peu de données sont initialement stockées.  Des concepts ont tout de même été introduits
même si le volume de données ne justifiait pas leur utilisation, comme par exemple la pagination des requêtes!  Lorsqu'il y a pagination
dans le repository, utilisez la pagination et lorsqu'il n'y en a pas, n'en utilisez pas.  Ne faites pas de modification à ce niveau!

Une fois le défi complété, vous pouvez soumettre votre projet en fichier .zip à l'adresse: http://voonyx.ca/fr/defi