package ca.voonyx.springbatch.challenge;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import ca.voonyx.springbatch.challenge.repository.CarRepository;

@EnableJpaRepositories(entityManagerFactoryRef = "voonyxChallengeEntityManagerFactory", transactionManagerRef = "voonyxChallengeTransactionManager")
public class DataSourceConfiguration {
	
    private Environment env;

	@Bean
	PlatformTransactionManager voonyxChallengeTransactionManager() {
		return new JpaTransactionManager(voonyxChallengeEntityManagerFactory().getObject());
	}

	@Bean
	LocalContainerEntityManagerFactoryBean voonyxChallengeEntityManagerFactory() {

		HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();

		LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();

		factoryBean.setDataSource(getMainDataSource());
		Properties props = new Properties();
	    props.put("hibernate.show_sql", env.getProperty("spring.jpa.show-sql"));
	    props.put("hibernate.hbm2ddl.auto", env.getProperty("spring.jpa.hibernate.ddl-auto"));
	    props.put("hibernate.dialect", env.getProperty("spring.jpa.database-platform"));
		factoryBean.setJpaProperties(props);
		factoryBean.setJpaVendorAdapter(jpaVendorAdapter);
		factoryBean.setPackagesToScan(CarRepository.class.getPackage().getName());

		return factoryBean;
	}

	// Cette datasource correspond au schema à partir duquel sont créées et lues les données du batch
	@Bean(name = "mainDataSource")
	public DataSource getMainDataSource() {
		return DataSourceBuilder.create().build();
	}

	// Cette datasource correspond au schema dans lequel SpringBatch écrit ses metadatas
	@Bean(name = "batchDataSource")
	@Primary
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSource getMetaDataSource() {
		return DataSourceBuilder.create().build();
	}
}
