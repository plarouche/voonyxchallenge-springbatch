package ca.voonyx.springbatch.challenge;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;

import ca.voonyx.springbatch.challenge.processor.DelearReportGenerationTasklet;
import ca.voonyx.springbatch.challenge.processor.HighValueCarsReportGenerationTasklet;

@PropertySource(value = { "classpath:application-h2.properties" }, ignoreResourceNotFound = true)
@EnableBatchProcessing
public class BatchConfiguration {
    
	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;
	
	@Autowired
	private DelearReportGenerationTasklet dealerReportTasklet;
	
	@Autowired
	private HighValueCarsReportGenerationTasklet highValueReportTasklet;

	@Bean
	public Job jobReports() {
		return jobBuilderFactory.get("JobReports")
				.incrementer(new RunIdIncrementer())
				.flow(stepGenerateDealerReport()).on(ExitStatus.COMPLETED.getExitCode()).end()
				.from(stepGenerateDealerReport()).on(ExitStatus.FAILED.getExitCode()).fail()
				.from(stepGenerateHighValueCarsReport()).on(ExitStatus.COMPLETED.getExitCode()).end()
				.from(stepGenerateHighValueCarsReport()).on(ExitStatus.FAILED.getExitCode()).fail()
				.end()
				.build();
	}
    
    @Bean
    public Step stepGenerateDealerReport() {
        return stepBuilderFactory.get("StepGenerateDealerReport")
        		.tasklet(dealerReportTasklet)
                .build();
    }
    
    @Bean
    public Step stepGenerateHighValueCarsReport() {
        return stepBuilderFactory.get("StepGenerateHighValueCarsReport")
        		.tasklet(highValueReportTasklet)
                .build();
    }
}
