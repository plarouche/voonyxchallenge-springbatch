package ca.voonyx.springbatch.challenge;

import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Application {

	public static void main(String[] args) throws Exception {
		ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);

        ExitCodeGenerator exitCodeGen = context.getBean(ExitCodeGenerator.class);

        int code = exitCodeGen.getExitCode();
        context.close();
        System.exit(code);
    }
}
