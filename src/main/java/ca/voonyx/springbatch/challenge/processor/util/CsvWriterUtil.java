package ca.voonyx.springbatch.challenge.processor.util;

import java.io.BufferedWriter;
import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.voonyx.springbatch.challenge.repository.model.Car;

public class CsvWriterUtil {

	private static final Logger logger = LoggerFactory.getLogger(CsvWriterUtil.class);
	
	static public void initOutputFile(String csvFileName) throws Exception {
		// On valide que le fichier à générer n'existe pas déjà
		File fileExist = new File(csvFileName);
		if (fileExist != null && fileExist.exists()) {
			fileExist.delete();
		}

		createDirectories(csvFileName);
	}
    
    static public void createDirectories(String csvFileName) throws Exception {
        File outputFolder = new File(csvFileName.substring(0, csvFileName.lastIndexOf("/")));
        if (!outputFolder.exists()) {
        	outputFolder.mkdirs();
        }
        logger.debug("Dossiers créés");
    }
	static public void writeHeader(BufferedWriter writer) throws Exception {
    	
    	// Écriture de l'entête du fichier
    	writer.write("Annee;Marque;Modele;Prix;Concessionnaire");
    	writer.write(System.lineSeparator());
    }
    
    static public void writeLine(BufferedWriter writer, Car car) throws Exception {
    	// Écriture d'une ligne dans le rapport
    	writer.write(car.getYear() + ";" + car.getMade() + ";" + car.getModel() + ";" + car.getPrice() + ";" + car.getDealer().getName());
    	writer.write(System.lineSeparator());
    }
}
