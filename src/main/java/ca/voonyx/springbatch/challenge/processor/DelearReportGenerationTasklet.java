package ca.voonyx.springbatch.challenge.processor;

import java.io.BufferedWriter;
import java.io.FileWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ca.voonyx.springbatch.challenge.processor.util.CsvWriterUtil;
import ca.voonyx.springbatch.challenge.repository.CarRepository;
import ca.voonyx.springbatch.challenge.repository.model.Car;

@Component
public class DelearReportGenerationTasklet implements Tasklet {
	
	@Value(value="${test.search.dealer.name}")
	private String dealerName;
	@Value(value="${test.output.dealer.report.filename}") 
	private String csvFileName;
	
	private static final Logger logger = LoggerFactory.getLogger(DelearReportGenerationTasklet.class);

	@Autowired
	private CarRepository carRepository;
	
	@Override
	@Transactional("voonyxChallengeTransactionManager")
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext context) throws Exception {

		int page = 0;
	    int pageSize = 2;
    	BufferedWriter csvWriter = null;
	    
	    try {
	    	
	    	CsvWriterUtil.initOutputFile(csvFileName);
			csvWriter = new BufferedWriter(new FileWriter(csvFileName));
			CsvWriterUtil.writeHeader(csvWriter);
	    	
	    	Pageable pageable = PageRequest.of(page++, pageSize);
	    	Page<Car> carPage = carRepository.findAllByDealer(dealerName, pageable);
		    
		    while(carPage.hasContent()) {
		    	for(Car car : carPage.getContent()) {
		    		CsvWriterUtil.writeLine(csvWriter, car);
		    	}
		    	
		    	carPage = carRepository.findAllByDealer(dealerName, pageable);
		    }
			
			stepContribution.setExitStatus(ExitStatus.COMPLETED);
	    } catch(Exception e) {
	    	logger.error(e.getMessage(), e);
	    	stepContribution.setExitStatus(ExitStatus.FAILED);
	    } finally {
			if(csvWriter != null) {
				csvWriter.close();
			}
		}
	    
		return RepeatStatus.FINISHED;

	}
}
