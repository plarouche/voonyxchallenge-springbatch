package ca.voonyx.springbatch.challenge.processor;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ca.voonyx.springbatch.challenge.processor.util.CsvWriterUtil;
import ca.voonyx.springbatch.challenge.repository.CarRepository;
import ca.voonyx.springbatch.challenge.repository.model.Car;

@Component
public class HighValueCarsReportGenerationTasklet implements Tasklet {
	
	@Value(value="${test.search.car.price}")
	private BigDecimal carPrice;
	@Value(value="${test.output.highvaluecars.report.filename}") 
	private String csvFileName;
	
	private static final Logger logger = LoggerFactory.getLogger(HighValueCarsReportGenerationTasklet.class);

	@Autowired
	private CarRepository carRepository;
	
	@Override
	@Transactional("voonyxChallengeTransactionManager")
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext context) throws Exception {

    	BufferedWriter csvWriter = null;
	    
	    try {
	    	
	    	CsvWriterUtil.initOutputFile(csvFileName);
			csvWriter = new BufferedWriter(new FileWriter(csvFileName));
			CsvWriterUtil.writeHeader(csvWriter);
	    	
	    	Set<Car> cars = carRepository.findAllByPriceGreaterThanEqual(carPrice);
		    
		    for(Car car : cars) {
		    	CsvWriterUtil.writeLine(csvWriter, car);
		    }
			
			stepContribution.setExitStatus(ExitStatus.COMPLETED);
	    } catch(Exception e) {
	    	logger.error(e.getMessage(), e);
	    	stepContribution.setExitStatus(ExitStatus.FAILED);
	    } finally {
			if(csvWriter != null) {
				csvWriter.close();
			}
		}
	    
		return RepeatStatus.FINISHED;

	}
}
