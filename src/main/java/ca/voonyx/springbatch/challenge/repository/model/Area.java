package ca.voonyx.springbatch.challenge.repository.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="REGION")
public class Area {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="areaSequence")
	@SequenceGenerator(name="areaSequence", sequenceName="areaSequence", allocationSize=1)
	@Column(name="ID", nullable=false)
	private Long id;
	
	@Column(name="NOM", nullable=false)
	private String name;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
