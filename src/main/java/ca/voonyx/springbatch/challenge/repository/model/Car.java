package ca.voonyx.springbatch.challenge.repository.model;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="AUTOMOBILE")
public class Car {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="carSequence")
	@SequenceGenerator(name="carSequence", sequenceName="carSequence", allocationSize=1)
	@Column(name="ID", nullable=false)
	private Long id;
	
	@Column(name="MARQUE", nullable=false, length=64)
	private String made;
	
	@Column(name="MODELE", nullable=false, length=64)
	private String model;
	
	@Column(name="PRIX_VENTE", nullable=false)
	private BigDecimal price;
	
	@Column(name="ANNEE", nullable=false)
	private int year;
	
	@Column(name="VENDU", nullable=false)
	private boolean sold;
	
	@ManyToOne(cascade = CascadeType.ALL, optional=false)
	private Dealer dealer;

    public Long getId() {
        return id;
    }

    public void setId(Long aId) {
        id = aId;
    }

    public String getMade() {
        return made;
    }

    public void setMade(String aMade) {
        made = aMade;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String aModel) {
        model = aModel;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal aPrice) {
        price = aPrice;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int aYear) {
        year = aYear;
    }

    public Dealer getDealer() {
        return dealer;
    }

    public void setDealer(Dealer aDealer) {
        dealer = aDealer;
    }

	public boolean isSold() {
		return sold;
	}

	public void setSold(boolean sold) {
		this.sold = sold;
	}
}
