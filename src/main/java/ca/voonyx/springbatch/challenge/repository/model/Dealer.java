package ca.voonyx.springbatch.challenge.repository.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CONCESSIONNAIRE")
public class Dealer {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="dealerSequence")
	@SequenceGenerator(name="dealerSequence", sequenceName="dealerSequence", allocationSize=1)
	@Column(name="ID", nullable=false)
	private Long id;
	
	@ManyToOne(cascade = CascadeType.ALL, optional=false)
	private Area area;
	
	@Column(name="NOM", nullable=false, length=255)
	private String name;
	
	@OneToMany(mappedBy = "dealer")
	private Set<Car> cars;

    public Long getId() {
        return id;
    }

    public void setId(Long aId) {
        id = aId;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area aArea) {
        area = aArea;
    }

    public String getName() {
        return name;
    }

    public void setName(String aName) {
        name = aName;
    }

    public Set<Car> getCars() {
        return cars;
    }

    public void setCars(Set<Car> aCars) {
        cars = aCars;
    }
}
