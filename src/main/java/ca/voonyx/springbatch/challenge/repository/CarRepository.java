package ca.voonyx.springbatch.challenge.repository;

import java.math.BigDecimal;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import ca.voonyx.springbatch.challenge.repository.model.Car;

public interface CarRepository extends JpaRepository<Car, Long> {

    public Page<Car> findAllByDealer(String dealerName, Pageable pageable);
    public Set<Car> findAllByPriceGreaterThanEqual(BigDecimal carPrice);
}
