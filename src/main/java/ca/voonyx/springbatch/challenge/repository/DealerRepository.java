package ca.voonyx.springbatch.challenge.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.voonyx.springbatch.challenge.repository.model.Area;
import ca.voonyx.springbatch.challenge.repository.model.Dealer;

public interface DealerRepository extends JpaRepository<Dealer, Long> {
    
    public List<Dealer> findAllByArea(Area area);
}
