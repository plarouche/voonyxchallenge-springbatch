package ca.voonyx.springbatch.challenge.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import ca.voonyx.springbatch.challenge.repository.model.Area;

public interface AreaRepository extends JpaRepository<Area, Long> {

}
